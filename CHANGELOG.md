# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 2019/01/02
### Changed
- [scritps](/scripts)
  - [config](/scripts/config)
  - [macos.sh](/scripts/macos.sh)
  - [macos-basic.sh](/scripts/macos-basic.sh)
  - [macos-network.sh](/scripts/macos-network.sh)
  - [macos-virsh.sh](/scripts/macos-virsh.sh)
  - [windows.sh](/scripts/windows.sh)
  - [windows-basic.sh](/scripts/windows-basic.sh)
  - [windows-network.sh](/scripts/windows-network.sh)
  - [windows-virsh.sh](/scripts/windows-virsh.sh)
- [README.md](/README.md)


## 2018/12/30
### Added
- [Wiki](https://gitlab.com/YuriAlek/vfio/wikis/home)
  - [Edit config and script](https://gitlab.com/YuriAlek/vfio/wikis/Edit-config-and-script)
  - [Home](https://gitlab.com/YuriAlek/vfio/wikis/Home)
  - [Install](https://gitlab.com/YuriAlek/vfio/wikis/Install)
  - [Troubleshoot](https://gitlab.com/YuriAlek/vfio/wikis/Troubleshoot)
  - [Use](https://gitlab.com/YuriAlek/vfio/wikis/Use)
  - [vbios](https://gitlab.com/YuriAlek/vfio/wikis/vbios)
- [scritps](/scripts)
  - [dnsmasq.conf](/scripts/dnsmasq.conf)
  - [macos.sh](/scripts/macos.sh)
  - [macos-basic.sh](/scripts/macos-basic.sh)
  - [macos-network.sh](/scripts/macos-network.sh)
  - [macos-virsh.sh](/scripts/macos-virsh.sh)
  - [smb.conf](/scripts/smb.conf)
  - [windows-basic.sh](/scripts/windows-basic.sh)
  - [windows-network.sh](/scripts/windows-network.sh)
  - [windows-virsh.sh](/scripts/windows-virsh.sh)
- [Changelog](/CHANGELOG.md)

### Changed
- [scripts](/scripts)
  - [config](/scripts/config)
  - [extract-vbios-linux.sh](/scripts/extract-vbios-linux.sh)
  - [extract-vbios-nvflash.sh](/scripts/extract-vbios-nvflash.sh)
  - [windows.sh](/scripts/windows.sh)
- [README.md](/README.md)

### Removed
- scripts
  - config-macos
  - macos-hs-install.md
  - macos-hs.sh
  - network.sh
  - qemu-mac@.service
  - qemu@.service
  - test-qemu.sh
  - windows-install.sh
- Troubleshoot.md [Moved to wiki](https://gitlab.com/YuriAlek/vfio/wikis/Troubleshoot)
- Install.md [Moved to wiki](https://gitlab.com/YuriAlek/vfio/wikis/Install)
